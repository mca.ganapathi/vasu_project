vpc_cidr = "10.10.0.0/16"
public_subnet_cidr = ["10.10.0.0/21","10.10.8.0/21"]
db_subnet_cidr = ["10.10.16.0/21","10.10.24.0/21"]
az = ["us-east-1a","us-east-1b"]
name = "vpc"
eks_name = "cluster"
