terraform {
  backend "s3" {
    bucket = "terraform-backend-statefile"
    key    = "vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

